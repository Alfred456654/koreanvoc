#!/bin/bash
DB_FILE="db_korean_voc.sqlite"
if [ -f "${DB_FILE}" ]; then
    echo "Local db found. [R]eplace, [B]ackup or [C]ancel?"
    read -r answer
    if [ "${answer}" = "B" ]; then
        BAK="${DB_FILE}.bak"
        rm -f "${BAK}"
        mv "${DB_FILE}" "${BAK}"
    elif [ "${answer}" = "R" ]; then
        rm -f "${DB_FILE}"
    else
        echo "Aborting..."
        exit 0
    fi
fi
touch "${DB_FILE}"
echo -e "CREATE TABLE VOCAB (FR TEXT NOT NULL, KO TEXT NOT NULL, LIST_NB INTEGER, CATEGORY TEXT, NB_ASK INTEGER NOT NULL DEFAULT 1, NB_OK INTEGER NOT NULL DEFAULT 0);" | sqlite3 "${DB_FILE}"
