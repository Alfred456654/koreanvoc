"""
SQL queries
"""
import sqlite3
from utils import void

# INSERT QUERIES

INSERT_WORD = "INSERT INTO VOCAB (FR, KO) VALUES (?, ?)"
INSERT_WORD_LN = "INSERT INTO VOCAB (FR, KO, LIST_NB) VALUES (?, ?, ?)"
INSERT_WORD_CAT = "INSERT INTO VOCAB (FR, KO, CATEGORY) VALUES (?, ?, ?)"
INSERT_WORD_LN_CAT = "INSERT INTO VOCAB (FR, KO, LIST_NB, CATEGORY) VALUES (?, ?, ?, ?)"

# SELECT QUERIES

# full_list
LIST_ALL_VOCABULARY = "SELECT * FROM VOCAB ORDER BY FR"
ALL_LISTS = "SELECT DISTINCT LIST_NB FROM VOCAB ORDER BY LIST_NB DESC"
ALL_CATEGORIES = "SELECT DISTINCT CATEGORY FROM VOCAB"

# quizz
QUIZZ = "SELECT FR, KO, ROWID FROM VOCAB WHERE 100*NB_OK/NB_ASK < (SELECT AVG(100*NB_OK/NB_ASK) " \
        "FROM VOCAB) ORDER BY RANDOM() LIMIT 1"
QUIZZ_LIST = "SELECT FR, KO, ROWID FROM VOCAB WHERE 100*NB_OK/NB_ASK < (SELECT AVG(" \
             "100*NB_OK/NB_ASK) FROM VOCAB WHERE LIST_NB = ?) AND LIST_NB = ? ORDER BY RANDOM() " \
             "LIMIT 1"
QUIZZ_LIST_FROM = "SELECT FR, KO, ROWID FROM VOCAB WHERE 100*NB_OK/NB_ASK < (SELECT AVG(" \
                  "100*NB_OK/NB_ASK) FROM VOCAB WHERE LIST_NB >= ?) AND LIST_NB >= ? ORDER BY RANDOM() " \
                  "LIMIT 1"
QUIZZ_CATEGORY = "SELECT FR, KO, ROWID FROM VOCAB WHERE 100*NB_OK/NB_ASK < (SELECT AVG(" \
                 "100*NB_OK/NB_ASK)FROM VOCAB WHERE CATEGORY = ?) AND CATEGORY = ? ORDER BY " \
                 "RANDOM() LIMIT 1"
QUIZZ_BACKUP = "SELECT FR, KO, ROWID FROM VOCAB ORDER BY RANDOM() LIMIT 1"
QUIZZ_LIST_BACKUP = "SELECT FR, KO, ROWID FROM VOCAB WHERE LIST_NB = ? ORDER BY RANDOM() LIMIT 1"
QUIZZ_LIST_FROM_BACKUP = "SELECT FR, KO, ROWID FROM VOCAB WHERE LIST_NB >= ? ORDER BY RANDOM() LIMIT 1"
QUIZZ_CATEGORY_BACKUP = "SELECT FR, KO, ROWID FROM VOCAB WHERE CATEGORY = ? ORDER BY RANDOM() " \
                        "LIMIT 1"
RANDOM_LIST_NB = "SELECT DISTINCT LIST_NB FROM VOCAB ORDER BY RANDOM() LIMIT 1"
SCORE_LIST = "SELECT 100*SUM(NB_OK)/SUM(NB_ASK) FROM VOCAB WHERE LIST_NB = ?"
SCORE_LIST_FROM = "SELECT 100*SUM(NB_OK)/SUM(NB_ASK) FROM VOCAB WHERE LIST_NB >= ?"
SCORE_CATEGORY = "SELECT 100*SUM(NB_OK)/SUM(NB_ASK) FROM VOCAB WHERE CATEGORY = ?"
SCORE_FULL = "SELECT 100*SUM(NB_OK)/SUM(NB_ASK) FROM VOCAB"

# quizz answer
LOOKUP_ANSWER_KO = "SELECT KO, ROWID FROM VOCAB WHERE FR = ?"
LOOKUP_ANSWER_FR = "SELECT FR, ROWID FROM VOCAB WHERE KO = ?"

# new word
NB_LAST_LIST_WORDS = "SELECT LIST_NB, COUNT(*) FROM VOCAB WHERE LIST_NB = (SELECT MAX(LIST_NB) " \
                     "FROM VOCAB)"

# stats
STATS = "SELECT COUNT(*), SUM(NB_ASK) - (SELECT COUNT(*) FROM VOCAB), SUM(NB_OK) FROM VOCAB"

# lists of lists
LOL_LISTS = "SELECT LIST_NB, 100*SUM(NB_OK)/SUM(NB_ASK), GROUP_CONCAT(FR, ', '), " \
            "GROUP_CONCAT(KO, ', ') FROM VOCAB WHERE LIST_NB IS NOT NULL GROUP BY LIST_NB ORDER BY LIST_NB DESC"
LOL_CATS = "SELECT CATEGORY, 100*SUM(NB_OK)/SUM(NB_ASK), GROUP_CONCAT(FR, ', ') AS FR, " \
           "GROUP_CONCAT(KO, ', ') AS KO FROM VOCAB WHERE CATEGORY IS NOT NULL GROUP BY CATEGORY"

# UPDATE QUERIES

RIGHT_ANSWER = "UPDATE VOCAB SET NB_OK = NB_OK + 1, NB_ASK = NB_ASK + 1 WHERE ROWID = ?"
WRONG_ANSWER = "UPDATE VOCAB SET NB_ASK = NB_ASK + 1 WHERE ROWID = ?"
RESET_LIST = "UPDATE VOCAB SET NB_ASK = 1, NB_OK = 0 where LIST_NB = ?"
RESET_CAT = "UPDATE VOCAB SET NB_ASK = 1, NB_OK = 0 where CATEGORY = ?"
RESET_ALL = "UPDATE VOCAB SET NB_ASK = 1, NB_OK = 0"
I_WAS_RIGHT = "UPDATE VOCAB SET NB_OK = NB_OK + 1 where ROWID = ?"
EDIT_WORD = "UPDATE VOCAB SET FR = ?, KO = ? WHERE FR = ? AND KO = ?"

# DELETE QUERIES

DELETE_WORD = "DELETE FROM VOCAB WHERE FR = ? AND KO = ?"


class SQLiteDAO:
    """
    Tool to make all of the queries to the DB
    """

    def __init__(self, db_file):
        """
        Constructor
        :param db_file: path to the SQLite DB
        """
        self.db_file = db_file

    def sql_insert(self, query, parameters):
        """
        Execute SQL Insert query
        :param query: the query (can be a prepared statement)
        :param parameters: the optional parameters to the query. Pass None if there are none.
        :return: nothing
        """
        sql_conn = sqlite3.connect(self.db_file)
        if void(parameters):
            sql_conn.execute(query)
        else:
            sql_conn.execute(query, parameters)
        sql_conn.commit()
        sql_conn.close()

    def sql_select(self, query, parameters, purge_empty=False):
        """
        Execute SQL Select query
        :param purge_empty:
        :param query: the query (can be a prepared statement)
        :param parameters: the optional parameters to the query. Pass None if there are none.
        :return: the results of the query, in a list of tuples
        """
        sql_conn = sqlite3.connect(self.db_file)
        if void(parameters):
            result = sql_conn.execute(query).fetchall()
        else:
            result = sql_conn.execute(query, parameters).fetchall()
        sql_conn.close()
        if purge_empty:
            result = [r[0] for r in result if r[0] is not None]
        return result

    def sql_select_fetchone(self, query, parameters=None):
        """
        Make a Select SQL query but only return one line
        :param parameters: self-explanatory...
        :param query: query to execute
        :return: one line of the result of the query
        """
        sql_conn = sqlite3.connect(self.db_file)
        if parameters:
            results = sql_conn.execute(query, parameters)
        else:
            results = sql_conn.execute(query)
        result = results.fetchone()
        sql_conn.close()
        return result

    def sql_delete(self, query, parameters):
        """
        Execute SQL Delete query
        :param query: query to execute (must be a prepared statement)
        :param parameters: parameters for the prepared statement
        :return: nothing
        """
        sql_conn = sqlite3.connect(self.db_file)
        _ = sql_conn.execute(query, parameters)
        sql_conn.commit()
        sql_conn.close()

    def sql_update(self, query, parameters=None):
        """
        Execute SQL Update query
        :param query: query to execute (must be a prepared statement)
        :param parameters: parameters for the prepared statement
        :return: nothing
        """
        sql_conn = sqlite3.connect(self.db_file)
        if parameters is None:
            _ = sql_conn.execute(query)
        else:
            _ = sql_conn.execute(query, parameters)
        sql_conn.commit()
        sql_conn.close()
