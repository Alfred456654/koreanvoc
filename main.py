"""
Entrypoint
"""
import os.path
import cherrypy
import config
from koreanvoc import KoreanVoc

if __name__ == '__main__':
    cherrypy.config.update({
        'server.socket_host': '0.0.0.0'
    })
    VOC_APP = KoreanVoc(config.DB_FILE)
    CONF = {
        '/': {
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './static'
        }
    }
    cherrypy.quickstart(VOC_APP, '/', CONF)
