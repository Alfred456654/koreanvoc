#!/usr/bin/env python3
import os
import sqlite3
import sys

DB_FILE = "db_korean_voc.sqlite"
CHECK = "SELECT * FROM VOCAB WHERE FR = ? AND KO = ? AND CATEGORY = ?"
QUERY = "INSERT INTO VOCAB (FR, KO, CATEGORY) VALUES (?, ?, ?) ON CONFLICT DO NOTHING"


def read_input(_filename):
    with open(_filename, 'r') as of:
        return [(csv.split(',')[0].strip(), csv.split(',')[1].strip().capitalize()) for csv in
                (list(map(str.strip, of.readlines())))]


def insert_new_word_in_db(_db_conn, _ko, _fr, _cat):
    parameters = (_fr, _ko, _cat)
    results = _db_conn.execute(CHECK, parameters)
    result = results.fetchone()
    if result is None:
        print(f"adding {_ko} -> {_fr}")
        _db_conn.execute(QUERY, parameters)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print(f"ERROR: Usage: {sys.argv[0]} csv_file category_name")
        sys.exit(1)
    filename = sys.argv[1]
    if not os.path.exists(filename):
        print(f"ERROR: '{filename}': file not found")
        sys.exit(1)
    db_conn = sqlite3.connect(DB_FILE)
    voc_tuples = read_input(filename)
    for voc_tuple in voc_tuples:
        insert_new_word_in_db(db_conn, voc_tuple[0], voc_tuple[1], sys.argv[2])
    db_conn.commit()
    db_conn.close()
