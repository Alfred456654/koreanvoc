"""
Miscellaneous methods
"""
from random import getrandbits


def xstr(in_str):
    """
    Transforms None to an empty string. Other inputs are returned without modifications.
    :param in_str: input string, can be None
    :return: the input string itself, or an empty string if the input was None
    """
    return '' if in_str is None else str(in_str)


def void(param):
    """
    Method to check that some variable is void (undefined, empty, zero, ...)
    :param param: any kind of variable
    :return: True if there is no relevant information in param, False otherwise
    """
    if isinstance(param, str):
        return len(param.strip()) == 0
    if isinstance(param, tuple):
        return len(param) == 0
    return not isinstance(param, int)


def one_in_four():
    """
    :return: True or False, by random
    """
    return not getrandbits(2)


def get_html_page_title(page):
    """
    Return whatever is in between the first occurrences of <title> and </title> in page
    :param page: path to the page
    :return: the title of the HTML page
    """
    with open(page, 'r') as open_file:
        html_content = ''.join(open_file.readlines())
        title_begin = html_content.find("<title>")
        title_end = html_content.find("</title>", title_begin)
        return html_content[title_begin + len("<title>"):title_end].strip()
