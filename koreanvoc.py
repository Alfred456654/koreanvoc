"""
App
"""
from glob import glob
import re
import cherrypy
import sql
import tpl
from utils import xstr, void, one_in_four, get_html_page_title
from config import MAX_WORDS_PER_LIST


def do_category_quizz(category, list_nb):
    if list_nb == "random":
        list_nb = ""
    query = sql.QUIZZ_CATEGORY
    alt_query = sql.QUIZZ_CATEGORY_BACKUP
    query_params = (category, category,)
    alt_query_params = (category,)
    return alt_query, alt_query_params, list_nb, query, query_params


def do_normal_quizz():
    query = sql.QUIZZ
    alt_query = sql.QUIZZ_BACKUP
    query_params = None
    alt_query_params = None
    return alt_query, alt_query_params, query, query_params


def do_list_quizz(list_nb, qfrom_flag):
    query1 = sql.QUIZZ_LIST
    query2 = sql.QUIZZ_LIST_BACKUP
    if qfrom_flag:
        query1 = sql.QUIZZ_LIST_FROM
        query2 = sql.QUIZZ_LIST_FROM_BACKUP
    params1 = (list_nb, list_nb,)
    params = (list_nb,)
    result = query2, params, list_nb, query1, params1
    alt_query, alt_query_params, list_nb, query, query_params = result
    return alt_query, alt_query_params, list_nb, query, query_params


class KoreanVoc:
    """
    CherryPy app
    """

    def __init__(self, db_file):
        """
        Constructor
        :param db_file: path to the SQLite DB
        """
        self.dao = sql.SQLiteDAO(db_file)

    @cherrypy.expose
    def new(self):
        """
        HTML Form to insert a new word in the database
        :return: the HTML form
        """
        result = tpl.NEW_WORD_FORM_TOP
        last_list_nb, nb_words_last_list = self.dao.sql_select_fetchone(sql.NB_LAST_LIST_WORDS)
        if nb_words_last_list >= MAX_WORDS_PER_LIST:
            last_list_nb += 1
        result += tpl.LIST_NB.format(last_list_nb, last_list_nb)
        categories = self.dao.sql_select(sql.ALL_CATEGORIES, None, purge_empty=True)
        html_cat = ''.join([tpl.CATEGORY.format(cat, cat) for cat in categories])
        html_cats = tpl.CATEGORIES.format(html_cat)
        result += html_cats
        result += tpl.NEW_WORD_FORM_BOTTOM
        return result

    @cherrypy.expose
    def insert_word(self, fr_word="", ko_word="", category="", list_nb="", new_category=""):
        """
        backend to actually insert the word in the DB
        :param fr_word: French translation
        :param ko_word: Korean translation
        :param category: Optional category of the word (verb, food, adjective...)
        :param list_nb: Belongs to vocabulary list #...
        :param new_category: In case no pre-existing category fit this new word
        :return: nothing, redirect to the form to insert another word
        """
        if void(fr_word) or void(ko_word):
            return tpl.ERROR_MESSAGE
        fr_word = fr_word.strip().capitalize()
        ko_word = ko_word.strip()
        query = sql.INSERT_WORD_LN
        parameters = (fr_word, ko_word)
        if void(category) and not void(new_category):
            category = new_category
        if void(category):
            if void(list_nb):
                list_nb, n_words = self.dao.sql_select_fetchone(sql.NB_LAST_LIST_WORDS)
                if n_words >= MAX_WORDS_PER_LIST:
                    list_nb += 1
            parameters += (list_nb,)
        else:
            query = sql.INSERT_WORD_CAT
            parameters += (category,)
        self.dao.sql_insert(query, parameters)
        raise cherrypy.HTTPRedirect("/new")

    @cherrypy.expose
    def list_all_voc(self):
        """
        Show all vocabulary
        :return: the HTML table with all the vocabulary
        """
        words = self.dao.sql_select(sql.LIST_ALL_VOCABULARY, None)
        result = tpl.LIST_TOP
        for word in words:
            values = [xstr(v) for v in list(word)]
            values = [values[0], values[1], values[2], values[2], values[3], values[3],
                      int(100 * int(values[5]) / int(values[4])),
                      tpl.EDIT_LINK.format(values[0], values[1])]
            result += tpl.LIST_LINE.format(*values)
        result += tpl.LIST_BOTTOM
        return result

    @cherrypy.expose
    def i_was_right(self, question, answer, list_nb="", category="", row_id="", qfrom_flag=""):
        """
        Count given answer as correct even when it was considered wrong
        :param question: self-explanatory
        :param answer: self-explanatory
        :param list_nb: self-explanatory
        :param category: self-explanatory
        :param row_id: ID in the DB
        :param qfrom_flag: if True, take all lists >= list_nb
        :return: an HTML page showing the success and the next question
        """
        self.dao.sql_update(sql.I_WAS_RIGHT, (row_id,))
        new_question, row_id, list_nb = self.make_quizz_form(category, list_nb, qfrom_flag == 'True')
        quizz_description = self.make_quizz_description(category, list_nb, qfrom_flag)
        result = tpl.SUCCESS.format(question, answer)
        result += tpl.QUIZZ_FORM.format(quizz_description, new_question, new_question, list_nb,
                                        category, row_id, qfrom_flag)
        result += tpl.FOOTER
        return result

    @cherrypy.expose
    def quizz_check_answer(self, question, answer, list_nb="", category="", row_id="", qfrom_flag=""):
        """
        Backend to check the provided answer to the quizz
        :param category: self-explanatory...
        :param list_nb: self-explanatory...
        :param question: the question
        :param answer: the provided answer
        :param row_id: the ID of the row whose score to update
        :param qfrom_flag: if True, take all lists >= list_nb
        :return: an HTML page showing either the success or the failure
        """
        if void(row_id):
            return tpl.ERROR_MESSAGE.format(f"no row_id provided for {question}")
        expected = self.dao.sql_select(sql.LOOKUP_ANSWER_FR, (question,), purge_empty=True)
        if len(expected) == 0:
            expected = self.dao.sql_select(sql.LOOKUP_ANSWER_KO, (question,), purge_empty=True)
        if len(expected) == 0:
            return tpl.ERROR_MESSAGE.format(f"no translation found for {question}")
        answer_sing = re.sub("s$", "", answer.strip().lower())
        answer_plural = answer_sing + 's'
        possible_answers = [e.strip().lower() for e in expected]
        if answer_sing in possible_answers or answer_plural in possible_answers:
            if len(expected) > 1:
                result = tpl.SUCCESS_ALT.format(question, answer, ', '.join(expected))
            else:
                result = tpl.SUCCESS.format(question, answer)
            self.dao.sql_update(sql.RIGHT_ANSWER, (row_id,))
        else:
            result = tpl.FAILURE.format(question, answer, ', '.join(expected),
                                        question, answer, list_nb, category, row_id, qfrom_flag)
            self.dao.sql_update(sql.WRONG_ANSWER, (row_id,))
        new_question, row_id, list_nb = self.make_quizz_form(category, list_nb, qfrom_flag == 'True')
        quizz_description = self.make_quizz_description(category, list_nb, qfrom_flag)
        result += tpl.QUIZZ_FORM.format(quizz_description, new_question, new_question, list_nb,
                                        category, row_id, qfrom_flag)
        result += tpl.FOOTER
        return result

    @cherrypy.expose
    def index(self, list_nb="random", category="", qfrom=""):
        """
        Main page: global quizz
        :return: HTML page for the global quizz
        """
        qfrom_flag = False
        if qfrom != "":
            list_nb = qfrom
            qfrom_flag = True
        question, row_id, list_nb = self.make_quizz_form(category, list_nb, qfrom_flag)
        quizz_description = self.make_quizz_description(category, list_nb, qfrom_flag)
        return tpl.QUIZZ.format(quizz_description, question, question, list_nb, category, row_id, qfrom_flag)

    def make_quizz_description(self, category, list_nb, qfrom_flag):
        """
        :param category: optional current category
        :param list_nb: optional current list number
        :param qfrom_flag: if True, take all lists >= list_nb
        :return: textual description of current quizz
        """
        if list_nb != "":
            query = sql.SCORE_LIST
            if qfrom_flag:
                query = sql.SCORE_LIST_FROM
            score = self.dao.sql_select_fetchone(query, (list_nb,))[0]
            quizz_description = f"List {list_nb} (score: {score}%)"
            if qfrom_flag:
                quizz_description = f"From {quizz_description}"
        elif category != "":
            inline_me = self.dao.sql_select_fetchone(sql.SCORE_CATEGORY, (category,))
            score = inline_me[0]
            quizz_description = f"Category '{category}' (score: {score}%)"
        else:
            score = self.dao.sql_select_fetchone(sql.SCORE_FULL)[0]
            quizz_description = f"Full quizz (score: {score}%)"
        return quizz_description

    def make_quizz_form(self, category, list_nb, qfrom_flag=False):
        """
        make html form quizz
        :param category: self-explanatory...
        :param list_nb: self-explanatory...
        :param qfrom_flag: if True, take all lists >= list_nb
        :return: html form quizz and list_nb
        """
        if category == "":
            if list_nb == "":
                alt_query, alt_query_params, query, query_params = do_normal_quizz()
            else:
                if list_nb == "random":
                    list_nb = self.dao.sql_select_fetchone(sql.RANDOM_LIST_NB)[0]
                alt_query, alt_query_params, list_nb, query, query_params = do_list_quizz(list_nb, qfrom_flag)
        else:
            alt_query, alt_query_params, list_nb, query, query_params = do_category_quizz(category, list_nb)
        try:
            fr_word, ko_word, row_id = self.dao.sql_select_fetchone(query, query_params)
        except TypeError:
            fr_word, ko_word, row_id = self.dao.sql_select_fetchone(alt_query, alt_query_params)
        if one_in_four():
            question = ko_word
        else:
            question = fr_word
        return question, row_id, list_nb

    @cherrypy.expose
    def delete_word(self, fr_word="", ko_word=""):
        """
        HTML form to delete a word
        :param fr_word: french translation of the word to delete
        :param ko_word: korean translation of the word to delete
        :return: HTML page
        """
        return tpl.DELETE_WORD.format(fr_word, ko_word, fr_word, ko_word)

    @cherrypy.expose
    def delete(self, fr_word="", ko_word=""):
        """
        Backend to delete a word from the db
        :param fr_word: self-explanatory
        :param ko_word: self-explanatory
        :return: nothing, redirect to list of words
        """
        self.dao.sql_delete(sql.DELETE_WORD, (fr_word, ko_word))
        raise cherrypy.HTTPRedirect("/list_all_voc")

    @cherrypy.expose
    def edit_word(self, fr_word="", ko_word=""):
        """
        HTML form to edit a word
        :param fr_word: french translation of the word to edit
        :param ko_word: korean translation of the word to edit
        :return: HTML page
        """
        return tpl.EDIT_WORD.format(fr_word, ko_word, fr_word, ko_word, fr_word, ko_word)

    @cherrypy.expose
    def edit(self, fr_word="", ko_word="", new_fr_word="", new_ko_word=""):
        """
        Backend to edit a word in the DB
        :param fr_word: french translation of the word to edit
        :param ko_word: korean translation of the word to edit
        :param new_fr_word: new french translation
        :param new_ko_word: new korean translation
        :return: nothing, redirect to list of words
        """
        if void(new_fr_word) or void(new_ko_word):
            return tpl.ERROR_MESSAGE
        new_fr_word = new_fr_word.strip().capitalize()
        new_ko_word = new_ko_word.strip()
        self.dao.sql_update(sql.EDIT_WORD, (new_fr_word, new_ko_word, fr_word, ko_word))
        raise cherrypy.HTTPRedirect("/list_all_voc")

    @cherrypy.expose
    def grammar(self):
        """
        List of grammar lessons
        :return: HTML page with links to grammar lessons
        """
        lessons = glob("static/grammar/*.html")
        result = tpl.GRAMMAR_TOP
        for lesson in lessons:
            result += tpl.GRAMMAR_LESSON.format(lesson, get_html_page_title(lesson))
        result += tpl.GRAMMAR_BOTTOM
        return result

    @cherrypy.expose
    def stats(self):
        """
        Quizz statistics
        :return: HTML for statistics page
        """
        nb_words, nb_questions, nb_ok = self.dao.sql_select_fetchone(sql.STATS, None)
        return tpl.STATS.format(nb_words, nb_questions, nb_ok, 100 * int(nb_ok) / int(nb_questions))

    @cherrypy.expose
    def voc_lists(self):
        """
        :return: All vocabulary, grouped by list number and by category
        """
        list_of_lists_r = self.dao.sql_select(sql.LOL_LISTS, None)
        list_of_lists = ""
        for list_nb in list_of_lists_r:
            list_of_lists += tpl.VOC_LIST_ENTRY_LN.format(list_nb[0], list_nb[0], list_nb[0], list_nb[0], list_nb[1],
                                                          list_nb[2], list_nb[3], list_nb[0])
        list_of_cats_r = self.dao.sql_select(sql.LOL_CATS, None)
        list_of_cats = ""
        for cat in list_of_cats_r:
            list_of_cats += tpl.VOC_LIST_ENTRY_CAT.format(cat[0], cat[0], cat[1], cat[2], cat[3],
                                                          cat[0])
        return tpl.VOC_LISTS.format(list_of_lists, list_of_cats)

    @cherrypy.expose
    def confirm_reset_list(self, list_nb):
        """
        :return: HTML page for confirming reset of stats on a list
        """
        return tpl.CONFIRM_RESET_LIST.format(list_nb, list_nb)

    @cherrypy.expose
    def confirm_reset_cat(self, category):
        """
        :return: HTML page for confirming reset of stats on a category
        """
        return tpl.CONFIRM_RESET_CAT.format(category, category)

    @cherrypy.expose
    def reset_list(self, list_nb):
        """
        :param list_nb: list on which stats will be reset
        :return: to list of lists
        """
        self.dao.sql_update(sql.RESET_LIST, (list_nb,))
        raise cherrypy.HTTPRedirect("/voc_lists")

    @cherrypy.expose
    def reset_cat(self, category):
        """
        :param category: category whose stats are being reset
        :return: to list of lists
        """
        self.dao.sql_update(sql.RESET_CAT, (category,))
        raise cherrypy.HTTPRedirect("/voc_lists")

    @cherrypy.expose
    def confirm_reset_all(self):
        """
        :return: HTML page for confirming reset of stats on everything
        """
        return tpl.CONFIRM_RESET_ALL

    @cherrypy.expose
    def reset_all_stats(self):
        """
        Reset all statistics
        :return: to list of lists
        """
        self.dao.sql_update(sql.RESET_ALL)
        raise cherrypy.HTTPRedirect("/voc_lists")
