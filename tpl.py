"""
HTML
"""

# Global #######################################################

HEADER = """<html>
<head>
    <title>{}</title>
    <link href="/static/style.css" rel="stylesheet">
    <script src="/static/korean.js"></script>
</head>
<body>
"""

MENU = """| <a href="/?list_nb=\"\"">Quizz (full)</a> |
<a href="/?list_nb=random">Quizz (random)</a> |
<a href="/new">New word</a> |
<a href="/list_all_voc">Full List</a> |
<a href="/voc_lists">List of lists</a> |
<a href="/grammar">Grammar</a> |
<a href="/stats">Stats</a> |"""

FOOTER = f"""
<hr/>
{MENU}</body>
</html>
"""

ERROR_MESSAGE = f"""
<p>Something went wrong, click <a href="/">HERE</a> to go back to the home page</p>
<p>{{}}</p>
{FOOTER}"""

# New word form ################################################

LIST_NB = f"""<tr><td>List NB: {{}}<input type="hidden" value="{{}}" name="list_nb" /></td><br />"""

CATEGORIES = f"""
<td>
    Category<br />
    <label><input type="radio" name="category" value=""/>
    <input type="text" name="new_category" /></label><br />
{{}}
</td></tr>
"""

CATEGORY = f"""<label>
<input type="radio" value="{{}}" name="category">{{}}</input>
</label><br />\n"""

NEW_WORD_FORM_TOP = f"""{HEADER.format("New word")}
<form method="post" action="insert_word">
    <div style="overflow-x:auto;">
    <table>
        <tr><td>Français</td><td><input type="text" name="fr_word" /></td></tr>
        <tr><td>한국어</td><td><input type="text" name="ko_word" /></td></tr>
"""

NEW_WORD_FORM_BOTTOM = f"""
    </table>
    </div>
    <button type="submit">Add</button>
</form>
{FOOTER}"""

# Quizz ########################################################

QUIZZ_FORM = f"""
<h3>{{}}</h3>
<p>{{}}</p>
<form method="post" action="quizz_check_answer">
    <input type="hidden" name="question" value="{{}}" />
    <p><input type="text" name="answer" autofocus/></p>
    <p><button type="submit">OK</button></p>
    <input type="hidden" name="list_nb" value="{{}}" />
    <input type="hidden" name="category" value="{{}}" />
    <input type="hidden" name="row_id" value="{{}}" />
    <input type="hidden" name="qfrom_flag" value="{{}}" />
</form>
"""

QUIZZ = f"""{HEADER.format("Quizz")}
{QUIZZ_FORM}
{FOOTER}"""

SUCCESS = f"""{HEADER.format("Correct!")}
<p class="green">Correct!</p>
<p>Question: <b>{{}}</b></p>
<p>Answer: <b>{{}}</b></p>
<hr/>
"""

SUCCESS_ALT = f"""{HEADER.format("Correct!")}
<p class="green">Correct!</p>
<p>Question: <b>{{}}</b></p>
<p>Answer: <b>{{}}</b></p>
<p>Possible answers: <b>{{}}</b></p>
<hr/>
"""

FAILURE = f"""{HEADER.format("Wrong")}
<p class="red">Wrong</p>
<p>Question: <b>{{}}</b></p>
<p>Your answer: <i>{{}}</i></p>
<p>Correct answer(s): <b><u>{{}}</u></b></p>
<form method="post" action="i_was_right">
    <input type="hidden" name="question" value="{{}}" />
    <input type="hidden" name="answer" value="{{}}" />
    <p><button type="submit">I was right</button></p>
    <input type="hidden" name="list_nb" value="{{}}" />
    <input type="hidden" name="category" value="{{}}" />
    <input type="hidden" name="row_id" value="{{}}" />
    <input type="hidden" name="qfrom_flag" value="{{}}" />
</form>
<hr/>
"""

# Global vocabulary list #######################################

LIST_TOP = f"""{HEADER.format("Vocabulary")}
{MENU}
<hr/>
<input type="text" id="search_filter" onkeyup="filterTable('voc_list')" placeholder="Filter" />
<div style="overflow-x:auto;">
    <table id="voc_list">
        <tr>
            <th>Français</th><th>한국어</th><th>List #</th><th>Category</th><th>%</th><th>Edit</th>
        </tr>
"""

LIST_LINE = """<tr style="display: none;">
<td>{}</td><td>{}</td><td><a href="/?list_nb={}">{}</a></td><td><a 
href="/?category={}">{}</a></td><td>{}</td><td>{}</td>
</tr>\n"""

LIST_BOTTOM = f"""  </table>
</div>
{FOOTER}"""

# Edit word ####################################################

EDIT_WORD = f"""{HEADER.format("Edit")}
<form method="POST" action="/edit">
    <input type="text" name="new_fr_word" value="{{}}"/>
    <input type="text" name="new_ko_word" value="{{}}"/>
    <input type="hidden" name="fr_word" value="{{}}"/>
    <input type="hidden" name="ko_word" value="{{}}"/>
    <button type="submit">OK</button>
</form>
<form method="POST" action="/delete_word">
    <input type="hidden" name="fr_word" value="{{}}"/>
    <input type="hidden" name="ko_word" value="{{}}"/>
    <button type="submit">Delete</button>
</form>
<p><a href="/list_all_voc">Cancel</a></p>
{FOOTER}
"""

EDIT_LINK = f"""<a href="/edit_word?fr_word={{}}&ko_word={{}}">✐</a>"""

# Delete word ##################################################

DELETE_WORD = f"""{HEADER.format("Delete")}
<p>Delete {{}} // {{}}?</p>
<form method="POST" action="/delete">
    <input type="hidden" name="fr_word" value="{{}}"/>
    <input type="hidden" name="ko_word" value="{{}}"/>
    <button type="submit">Confirm delete</button>
</form>
<p><a href="/list_all_voc">Cancel</a></p>
{FOOTER}
"""

# Grammar ######################################################

GRAMMAR_TOP = f"""{HEADER.format("Grammar")}
<ul>"""

GRAMMAR_LESSON = f"""<li><a href="{{}}">{{}}</a></li>"""

GRAMMAR_BOTTOM = f"""</ul>
{FOOTER}
"""

# Stats ########################################################

STATS = f"""{HEADER.format("Statistics")}
<p>Number of words: <b>{{}}</b></p>
<p>Questions asked: <b>{{}}</b></p>
<p>Questions correctly answered: <b>{{}}</b></p>
<p>Percent correct: <b>{{:.2f}}%</b></p>
{FOOTER}
"""

# Vocabulary lists #############################################

VOC_LISTS_LN = f"""<table id="list_of_lists"><tr><th>List Nb</th><th>Score</th><th>Français</th>
<th>한국어</th><th>♻</th></tr>{{}}</table>"""

VOC_LIST_ENTRY_LN = f"""<tr>
<td><a href="/?list_nb={{}}">{{}}</a><br/><a href="/?qfrom={{}}">{{}}+</a></td><td>{{}}</td><td>{{}}</td>
<td>{{}}</td><td><a href="/confirm_reset_list?list_nb={{}}">♻</a></td>
</tr>"""

VOC_LISTS_CAT = f"""<table><tr><th>Category</th><th>Score</th><th>Français</th><th>한국어</th>
<th>♻</th></tr>{{}}</table>"""

VOC_LIST_ENTRY_CAT = f"""<tr>
<td><a href="/?category={{}}">{{}}</a></td><td>{{}}</td>
<td>{{}}</td><td>{{}}</td><td><a href="/confirm_reset_cat?category={{}}">♻</a></td>
</tr>"""

VOC_LISTS = f"""{HEADER.format("Lists of lists")}
{MENU}
<hr/>
<p><a href="#lists">List numbers</a></p>
<p><a href="#categories">Categories</a></p>
<hr/>
<input type="text" id="search_filter" onkeyup="filterTable('list_of_lists')" placeholder="Filter" />
<div id="lists"><p>List numbers</p></div>
<p><a href="/confirm_reset_all">Reset stats for all words</a></p>
{VOC_LISTS_LN}
<div id="categories"><p>Categories</p></div>
{VOC_LISTS_CAT}
{FOOTER}
"""

# Reset list/category ##########################################

CONFIRM_RESET_LIST = f"""{HEADER.format("Reset list stats?")}
<p>Reset list {{}} stats?</p>
<form method="POST" action="/reset_list">
    <input type="hidden" name="list_nb" value="{{}}"/>
    <button type="submit">Yes</button>
</form>
<p><a href="/voc_lists">Cancel</a></p>
"""

CONFIRM_RESET_CAT = f"""{HEADER.format("Reset category stats?")}
<p>Reset category {{}} stats?</p>
<form method="POST" action="/reset_cat">
    <input type="hidden" name="category" value="{{}}"/>
    <button type="submit">Yes</button>
</form>
<p><a href="/voc_lists">Cancel</a></p>
"""

CONFIRM_RESET_ALL = f"""{HEADER.format("Reset all stats?")}
<p>Reset all stats?</p>
<form method="POST" action="/reset_all_stats">
    <button type="submit">Yes</button>
</form>
<p><a href="/voc_lists">Cancel</a></p>
"""
