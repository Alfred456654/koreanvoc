function filterTable(div_id) {
    let fr_fld, ko_fld;
    if (div_id === "voc_list") {
        fr_fld = 0;
        ko_fld = 1;
    } else if (div_id === "list_of_lists") {
        fr_fld = 2;
        ko_fld = 3;
    }
    let input, filter, table, tr, i, txtValue, td_fr, td_ko, txtValue2;
    input = document.getElementById("search_filter");
    filter = input.value.toLowerCase();
    table = document.getElementById(div_id);
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        if (filter.length == 0 && div_id === "voc_list") {
            tr[i].style.display = "none";
        } else {
            td_fr = tr[i].getElementsByTagName("td")[fr_fld];
            td_ko = tr[i].getElementsByTagName("td")[ko_fld];
            if (td_fr && td_ko) {
                txtValue = td_fr.textContent || td_fr.innerText;
                if (txtValue.toLowerCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    txtValue2 = td_ko.textContent || td_ko.innerText;
                    if (txtValue2.toLowerCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    }
}
